import 'package:flutter/material.dart';

void main() {runApp(MyApp());}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Homework 1',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 1'),
        ),
        body: Center(
          child: Text("Name: Alexander Bermudez \n"
              "Graduation Date: Spring 2021 \n"
              "Favorite Quote: 'You miss 100% of the\nshots you don't take.'\n"
              "-Wayne Gretsky -Michael Scott"),
        ),
      ),
    );
  }
}